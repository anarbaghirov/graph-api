import java.sql.{Connection, DriverManager, ResultSet}

import StorageManager.Tables.Table

object StorageManager {
  private def CONNECTION_STRING = "jdbc:postgresql://localhost:5432/public?user=postgres&password=postgres"

  object Tables extends Enumeration {
    type Table = Value

    val Clusters: StorageManager.Tables.Value = Value("clusters")
  }

  def getConnection: Connection = {
    classOf[org.postgresql.Driver]

    DriverManager.getConnection(CONNECTION_STRING)
  }

  def getGraph(connection: Connection): GraphAPI.Graph = {
    val statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)
    val result = statement.executeQuery(
      """
        |select n1, n2 from edges
        |union
        |select n2, n1 from edges
        |""".stripMargin)
    var graph: GraphAPI.Graph = Map()

    while (result.next()) {
      val src = result.getInt("n1")
      val dst = result.getInt("n2")

      graph = graph.updated(src, graph.getOrElse(src, Set()) + dst)
    }

    statement.close()

    graph
  }

  def saveComponents(connection: Connection, components: GraphAPI.Components): Unit = {
    val query = "insert into clusters values(?, ?)"
    val statement = connection.prepareStatement(query)

    components.foreach(component => {
      statement.setInt(1, component._1)
      statement.setInt(2, component._2)
      statement.addBatch()
    })

    statement.executeBatch()
    statement.close()
  }

  def truncateTable(connection: Connection, table: Table): Unit = {
    val query = s"truncate table ${ table }"
    val statement = connection.createStatement()

    statement.execute(query)
    statement.close()
  }

  def truncateClusters(connection: Connection): Unit = truncateTable(connection, StorageManager.Tables.Clusters)
}
