object Main extends App {
  val connection = StorageManager.getConnection
  val graph = StorageManager.getGraph(connection)
  val components = GraphAPI.getComponents(graph)

  StorageManager.truncateClusters(connection)
  StorageManager.saveComponents(connection, components)

  connection.close()
}
