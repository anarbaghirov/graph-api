object GraphAPI {
  type Components = Map[Int, Int]
  type Neighbours = Set[Int]
  type Graph = Map[Int, Neighbours]

  @scala.annotation.tailrec
  def getComponents(graph: Graph, components: Components = Map(), component: Int = -1): Components = {
    @scala.annotation.tailrec
    def markNeighbours(graph: Graph, neighbours: Neighbours, components: Components): Components = {
      if (neighbours.isEmpty)
        components
      else if (components.contains(neighbours.head)) graph.get(neighbours.head) match {
        case Some(otherNeighbours) =>
          markNeighbours(graph, neighbours.tail ++ otherNeighbours.filterNot(components.contains), components)
        case None =>
          markNeighbours(graph, neighbours.tail, components)
      }
      else
        markNeighbours(graph, neighbours, components.updated(neighbours.head, component))
    }

    if (graph.isEmpty)
      components
    else if (components.contains(graph.head._1))
      getComponents(graph.tail, components ++ markNeighbours(graph.tail, graph.head._2, components), component)
    else
      getComponents(graph, components.updated(graph.head._1, component + 1), component + 1)
  }
}
